import React from 'react';
import ReactDOM from 'react-dom';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";


// Components 
import { NavigationBar } from './components/NavBar/NavigationBar';




// what i want to render
const App = () => {
  return (
    <div>
      <NavigationBar/>
    </div>

  )
}



// tell react to render 
ReactDOM.render(<App/>,document.querySelector('#root'))