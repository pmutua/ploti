from datatime import datetime

def get_timestamp():
    unformatted_time = datetime.datetime.now()
    formated_time = unformatted_time.strftime("%Y%m%d%H%M%S")
    return formatted_time
    