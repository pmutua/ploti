from rest_framework.serializers import ModelSerializer
from mpesa.models import LNMpesaOnline

class LNMpesaOnlinerSerializer(ModelSerializer):
    model = LNMpesaOnline
    fields = '__all__'
