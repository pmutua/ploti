from django.db import models

# Create your models here.


class LNMpesaOnline(models.Model):
    MpesaReceiptNumber = models.CharField(max_length=50)
    Amount = models.FloatField()
    ResultDesc = models.CharField(max_length=120)
    ResultCode = models.IntegerField()
    CheckoutRequestID = models.CharField(max_length=50)
    MerchantRequestID = models.CharField(max_length=50)
    PhoneNumber =  models.CharField(max_length=9)
    Balance =  models.CharField(max_length=50,blank=True,null=True)
    TransactionDate = models.DateTimeField()
    
