from encode import generate_password
from utils import get_timestamp
import keys

def lipa_na_mpesa():
    formatted_time = get_timestamp()
    decoded_password = generate_password.formated_time 
    access_token = generate_access_token

    api_url = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials"

    headers = {"Authorization": "Bearer %s" % access_token}
    request = {
        "BusinessShortCode": " ",
        "Password": " ",
        "Timestamp": formatted_time,
        "TransactionType": "CustomerPayBillOnline",
        "Amount": "1",
        "PartyA": " ",
        "PartyB": " ",
        "PhoneNumber": " ",
        "CallBackURL": "https://ip_address:port/callback",
        "AccountReference": " ",
        "TransactionDesc": " "
    }
    
    response = requests.post(api_url, json = request, headers=headers)
    
    print (response.text)
  