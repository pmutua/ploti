from django.contrib.auth.models import User
from mpesa.apiv1.serializers import  LNMpesaOnlinerSerializer,C2BPaymentSerializer
from mpesa.models import *
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from  datetime import datetime 
# from rest_framework.permissions import IsAdminUser

class LNMpesaOnlineCallBackUrlAPIView(CreateAPIView):
    queryset = LNMpesaOnline.objects.all()
    serializer_class = LNMpesaOnlinerSerializer

    def create(self,request):
        data = request.data
        print(request.data)
        merchant_request_id = data["Body"]["stkCallback"]["MerchantRequestID"]
        checkout_request_id = data["Body"]["stkCallback"]["CheckoutRequestID"]
        result_code = data["Body"]["stkCallback"]["ResultCode"]
        result_description = data["Body"]["stkCallback"]["ResultDesc"]

        callbackmetadata = data["Body"]["stkCallback"]["CallbackMetadata"]["Item"]

        for item in callbackmetadata:
            if item['Name'] == 'Amount':
                amount = item['Value']
            elif item['Name'] == 'MpesaReceiptNumber':
                mpesa_recipt_no = item['Value']
            elif  item['Name'] == 'TransactionDate':
                transaction_date = item['Value']
            elif item['Name'] == 'PhoneNumber':
                  phone_number  = item['Value'] 
            else:
                pass


        import pytz
        
        conv_to_string = str(transaction_date)
        converted_trans_date = datetime.strptime(conv_to_string,"%Y%m%d%H%M%S")
        # Make time zone aware 
        aware_transaction_time = pytz.utc.localize(converted_trans_date)

        obj = LNMpesaOnline.objects.create(
            MpesaReceiptNumber = mpesa_recipt_no,
            Amount = amount,
            ResultDesc = result_description,
            ResultCode = result_code,
            CheckoutRequestID = checkout_request_id,
            MerchantRequestID = merchant_request_id,
            PhoneNumber = phone_number,
            TransactionDate = aware_transaction_time
    
        )
        
        serialized_data  = LNMpesaOnlinerSerializer(obj).data
        return Response(serialized_data)
    

class C2BValidationAPIView(CreateAPIView):
    queryset = C2BPayments.objects.all()
    serializer_class = C2BPaymentSerializer
    permission_classes = [AllowAny]

    # def create(self, request):
    #     print(request.data, "this is request.data in Validation")

    #     from rest_framework.response import Response
    #     my_headers = self.get_success_headers(request.data)

    #     return Response({
    #         "ResultCode": 1,
    #         "ResponseDesc":"Failed!"
    #     },
    #     headers=my_headers)

class C2BConfirmationAPIView(CreateAPIView):
    queryset = C2BPayments.objects.all()
    serializer_class = C2BPaymentSerializer
    permission_classes = [AllowAny]

    # def create(self, request):
    #     print(request.data, "this is request.data in Confirmation")

    #     """
    #     {'TransactionType': 'Pay Bill', 
    #     'TransID': 'NCQ61H8BK4',
    #      'TransTime': '20190326210441',
    #       'TransAmount': '2.00', 
    #       'BusinessShortCode': '601445',
    #        'BillRefNumber': '12345678', 
    #        'InvoiceNumber': '', 
    #        'OrgAccountBalance': '18.00', 
    #        'ThirdPartyTransID': '', 
    #        'MSISDN': '254708374149', 
    #        'FirstName': 'John', 
    #        'MiddleName': 'J.', 
    #        'LastName': 'Doe'
    #        } 
    #        this is request.data in Confirmation
    #        """


    #     from rest_framework.response import Response

    #     return Response({"ResultDesc": 0})