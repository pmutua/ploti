from django.contrib.auth.models import User
from mpesa.serializers import  LNMpesaOnlinerSerializer
from mpesa.models import *
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
import datetime from datetime 
# from rest_framework.permissions import IsAdminUser

class LNMpesaOnlineCallBackUrlAPIView(CreateAPIView):
    queryset = LNMpesaOnline.objects.all()
    serializer_class = LNMpesaOnlinerSerializer

    def create(self,request):
        data = request.data

        merchant_request_id = data["Body"]["stkCallback"]["MerchantRequestID"]
        checkout_request_id = data["Body"]["stkCallback"]["CheckoutRequestID"]
        result_code = data["Body"]["stkCallback"]["ResultCode"]
        result_description = data["Body"]["stkCallback"]["ResultDesc"]
        result_description = data["ResultCode"]
        amount = data["CallBackMetadata"]["Item"][0]["Value"]
        balance = data["CallBackMetadata"]["Item"][0]["Balance"]
        mpesa_recipt_no = data["CallBackMetadata"]["Item"][0]["MpesaReceiptNumber"]
        transaction_date = data["CallBackMetadata"]["Item"][0]["TransactionDate"]
        phone_number = data["CallBackMetadata"]["Item"][0]["PhoneNumber"]

        import pytz
        
        conv_to_string = str(transaction_date)
        converted_trans_date = datetime.strptime(conv_to_string,"%Y%m%d%H%M%S")
        # Make time zone aware 
        aware_transaction_time = pytz.utc.localize(converted_trans_date)

        obj = LNMpesaOnline.objects.create(
            MpesaReceiptNumber = mpesa_recipt_no,
            Amount = amount,
            ResultDesc = result_description,
            ResultCode = result_code,
            CheckoutRequestID = checkout_request_id,
            MerchantRequestID = merchant_request_id,
            PhoneNumber = phone_number,
            Balance = balance,
            TransactionDate = aware_transaction_time
    
        )
        
        serialized_data  = LNMpesaOnlinerSerializer(obj).data
        return Response(serialized_data)
    