from django.contrib import admin
from django.urls import include, path
from mpesa.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('lnm/', LNMpesaOnlineCallBackUrlAPIView.as_view(),name='lnm-callback')
]
